/**
 *       @file  SradOCL_exc.h
 *      @brief  The SradOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Mirko Conti mirko.conti1@mail.polimi.it
 *     @author	Michele Zanella zanella.michele@mail.polimi.it
 *
 *     Company  Polytechnic University of Milan
 *   Copyright  Copyright (c) 2014
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef SRADOCL_EXC_H_
#define SRADOCL_EXC_H_

#include <bbque/bbque_exc.h>

//Application include
#include <main.h>
#include <CL/cl.h>					// (in path specified to compiler) needed by OpenCL types and functions
#include "./../src/util/simple-opencl/simpleCL.h"
#include "./../src/util/opencl/opencl.h"		// (in directory) needed by device functions

// The highest id number for AWM featuring only the CPU
#define AWM_ID_MAX_CPU 7

using bbque::rtlib::BbqueEXC;

class SradOCL : public BbqueEXC {

public:

	SradOCL(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			fp* image,
			int Nr,
			int Nc,
			long Ne,
			int niter,				
			fp lambda,				
			long NeROI,
			int* iN,
			int* iS,
			int* jE,
			int* jW,
			int iter,
			int mem_size_i,
			int mem_size_j,
			int wg_size,
			int awm_id);

private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();
	
	void displayTiming();
	void allocateMemory();
	void copyInputToDevice();
	void kernelsSetArguments();
	void extractKernel();	
	void prepareKernel();	
	void reduceKernel();
	void sradKernel();
	void srad2Kernel();
	void compressKernel();

	//=====================================================================
	// APPLICATION VARIABLES
	//=====================================================================

	// common variables
	cl_int error;
	long no;
	int mul;
	int mem_size_single = sizeof(fp) * 1;
	// error = clFinish(hardware.queue);
	fp total;
	fp total2;
	fp meanROI;
	fp meanROI2;
	fp varROI;
	fp q0sqr;
	cl_mem d_iN,d_iS, d_jE, d_jW, d_dN, d_dS, d_dW, d_dE, d_c, d_sums,d_sums2,d_I;
	int blocks_x;			// CUDA kernel execution parameters
	int mem_size;			// matrix memory size
	fp* image;			// input image
	int Nr;				// IMAGE nbr of rows
	int Nc;				// IMAGE nbr of cols
	long Ne;			// IMAGE nbr of elem
	int niter;			// nbr of iterations			
	fp lambda;			// update step size	
	long NeROI;			// ROI nbr of elements
	int* iN;
	int* iS;
	int* jE;
	int* jW;
	int iter;			// primary loop
	int mem_size_i;
	int mem_size_j;
	int wg_size;
	int awm_id;
	
	// SimpleOCL variables
	sclHard hardware;

	sclSoft extract_software, prepare_software, reduce_software, srad_software,
				srad2_software, compress_software;

	// threads
	size_t local_work_size[1];

	// workgroups
	int blocks_work_size;
	size_t global_work_size[1];
	int blocks2_x;
	int blocks2_work_size;
	size_t global_work_size2[1];

	// timers
	long long time0;
	long long time1;
	long long time2;
	long long time3;
	long long time4;
	long long time5;
	
};

#endif // SRADOCL_EXC_H_

