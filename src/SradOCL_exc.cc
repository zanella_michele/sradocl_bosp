/**
 *       @file  SradOCL_exc.cc
 *      @brief  The SradOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Mirko Conti mirko.conti1@mail.polimi.it
 *     @author	Michele Zanella zanella.michele@mail.polimi.it
 *
 *     Company  Polytechnic University of Milan
 *   Copyright  Copyright (c) 2014
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "SradOCL_exc.h"

#include <cstdio>
#include <bbque/utils/utility.h>

//Application include
#include <stdlib.h>
#include <main.h>
#include <CL/cl.h>				// (in path specified to compiler) needed by OpenCL types and functions
#include "./util/simple-opencl/simpleCL.h"
#include "./util/opencl/opencl.h"		// (in directory) needed by device functions
#include "./util/timer/timer.h"			// (in specified path)
#include "bbque/config.h"

SradOCL::SradOCL(std::string const & name,	//std BOSP parameter
		std::string const & recipe,	//std BOSP parameter
		RTLIB_Services_t *rtlib,	//std BOSP parameter
		fp* image,			// input image
		int Nr,				// IMAGE nbr of rows
		int Nc,				// IMAGE nbr of cols
		long Ne,			// IMAGE nbr of elem
		int niter,			// nbr of iterations
		fp lambda,			// update step size
		long NeROI,			// ROI nbr of elements
		int* iN,
		int* iS,
		int* jE,
		int* jW,
		int iter,			// primary loop
		int mem_size_i,
		int mem_size_j,
		int wg_size,
		int awm_id) :
	BbqueEXC(name, recipe, rtlib) {

	logger->Warn("New SradOCL::SradOCL()");

	// Instantiation parameters
	this->image = image;
	this->Nr = Nr;
	this->Nc = Nc;
	this->Ne = Ne;
	this->niter = niter;
	this->lambda = lambda;
	this->NeROI = NeROI;
	this->iN = iN;
	this->iS = iS;
	this->jE = jE;
	this->jW = jW;
	this->iter = iter;
	this->mem_size_i = mem_size_i;
	this->mem_size_j = mem_size_j;
	this->wg_size    = wg_size;
	this->awm_id     = awm_id;

	logger->Info("EXC Unique IDentifier (UID): %u", GetUid());

}

RTLIB_ExitCode_t SradOCL::onSetup() {
	logger->Warn("SradOCL::onSetup()");

	if (awm_id < 0)
		return RTLIB_OK;

	RTLIB_Constraint_t constr_set = {
		awm_id,
		CONSTRAINT_ADD,
		UPPER_BOUND
	};
	SetConstraints(&constr_set, 1);
	logger->Notice("Forcing Application Working Mode [%2d]", awm_id);
	return RTLIB_OK;
}

RTLIB_ExitCode_t SradOCL::onConfigure(int8_t awm_id) {

	logger->Warn("SradOCL::onConfigure(): EXC [%s] => AWM [%02d]",
			exc_name.c_str(), awm_id);

	//=====================================================================
	// OPENCL ENVIRONMENT CONFIGURATION
	//=====================================================================

	// local variables
	char stdPath[]=BBQUE_PATH_PREFIX"""/usr/bin/srad_kernels/kernel_gpu_opencl.cl";
	sclHard* hardware_list;
	int found;

	// SIMPLE OPENCL CALLS
	//=====================================================================
	time0 = get_time();
	//Get the hardware
	hardware_list = sclGetAllHardware(&found);
	hardware = hardware_list[0];
	if (!found) {
		logger->Error("No Platform/device available [%p]", hardware_list);
		return RTLIB_ERROR;
	}

	logger->Warn("Hardware OK! Getting software...");
	time1 = get_time();
	//Get the software
	extract_software = sclGetCLSoftware(stdPath,
			"extract_kernel", hardware);
	logger->Warn("Extract Software OK: 20%\r");
	prepare_software = sclGetCLSoftware(stdPath,
			"prepare_kernel", hardware);
	logger->Warn("Prepare Software OK: 40%\r");
	reduce_software = sclGetCLSoftware(stdPath,
			"reduce_kernel", hardware);
	logger->Warn("Reduce Software OK: 60%\r");
	srad_software = sclGetCLSoftware(stdPath,
			"srad_kernel", hardware);
	logger->Warn("SRAD Software OK: 80%\r");
	srad2_software = sclGetCLSoftware(stdPath,
			"srad2_kernel", hardware);
	logger->Warn("SRAD2 Software OK: 90%\r");
	compress_software = sclGetCLSoftware(stdPath,
			"compress_kernel", hardware);
	logger->Warn("Software OK!");

	// ALLOCATE MEMORY
	//=====================================================================
	time2 = get_time();
	allocateMemory();
	time3 = get_time();

	// KERNEL EXECUTION PARAMETERS
	//=====================================================================

	// Workgroup size
	if ((wg_size > 0) || (awm_id < 0))
		local_work_size[0] = wg_size;
	else
		local_work_size[0] = (NUMBER_THREADS * (awm_id + 1))/(AWM_ID_MAX_CPU + 1);

	blocks_x = Ne / (int) local_work_size[0];
	if (Ne % (int) local_work_size[0] != 0) {	// compensate for division remainder above by adding one grid
		blocks_x = blocks_x + 1;
	}
	blocks_work_size = blocks_x;
	global_work_size[0] = blocks_work_size * local_work_size[0];	// define the number of blocks in the grid

	logger->Warn("max # of workgroups = %d, # of threads/workgroup = %d (ensure that device can handle)\n",
			(int) (global_work_size[0] / local_work_size[0]),
			(int) local_work_size[0]);

	time4 = get_time();
	kernelsSetArguments();
	time5 = get_time();
	//=====================================================================

	// DISPLAY TIMING
	displayTiming();

	//=====================================================================
	// COPY INPUT TO DEVICE
	//=====================================================================
	copyInputToDevice();

	//=====================================================================
	// EXECUTING KERNELS
	//=====================================================================

	//=====================================================================
	// EXTRACT KERNEL - SCALE IMAGE DOWN FROM 0-255 TO 0-1 AND EXTRACT
	// This is done once
	//=====================================================================
	extractKernel();

	return RTLIB_OK;
}

RTLIB_ExitCode_t SradOCL::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();
	logger->Warn("SradOCL::onRun()  : EXC [%s]  @ AWM [%02d], Cycle [%4d]",
			exc_name.c_str(), wmp.awm_id, Cycles());

	if (Cycles() >= niter) {
		//=====================================================================
		// COMPRESS KERNEL - SCALE IMAGE UP FROM 0-1 TO 0-255 AND COMPRESS
		//=====================================================================
		compressKernel();

		//=====================================================================
		// 	COPY RESULTS BACK TO CPU
		//=====================================================================
		error = clEnqueueReadBuffer(hardware.queue, d_I, CL_TRUE, 0, mem_size,
				image, 0, NULL, NULL);
		if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
		return RTLIB_EXC_WORKLOAD_NONE;
	}
	iter = Cycles();
	fflush(NULL);

	//=====================================================================
	// PREPARE KERNEL
	//=====================================================================
	prepareKernel();

	//=====================================================================
	// REDUCE KERNEL - performs subsequent reductions of sums
	//=====================================================================
	reduceKernel();

	// copy total sums to device
	error = clEnqueueReadBuffer(hardware.queue, d_sums, CL_TRUE, 0,
			mem_size_single, &total, 0, NULL, NULL);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	error = clEnqueueReadBuffer(hardware.queue, d_sums2, CL_TRUE, 0,
			mem_size_single, &total2, 0, NULL, NULL);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	// calculate statistics
	meanROI  = total / (fp) (NeROI);		// gets mean (average) value of element in ROI
	meanROI2 = meanROI * meanROI;
	varROI   = (total2 / (fp) (NeROI)) - meanROI2;	// gets variance of ROI
	q0sqr    = varROI / meanROI2;			// gets standard deviation of ROI

	//=====================================================================
	// SRAD KERNEL
	//=====================================================================
	sradKernel();

	//=====================================================================
	// SRAD2 KERNEL
	//=====================================================================
	srad2Kernel();

	return RTLIB_OK;
}

RTLIB_ExitCode_t SradOCL::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Debug("SradOCL::onMonitor()  : EXC [%s]  @ AWM [%02d], Cycle [%4d]",
		exc_name.c_str(), wmp.awm_id, Cycles());

	return RTLIB_OK;
}

RTLIB_ExitCode_t SradOCL::onRelease() {

	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Warn("Srad::onRelease()  : EXC [%s]",
		exc_name.c_str());

	//=====================================================================
	// FREE MEMORY
	//=====================================================================

	// OpenCL structures
	error = clReleaseKernel(extract_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(prepare_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(reduce_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(srad_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(srad2_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(compress_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(extract_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(prepare_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(reduce_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(srad_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(srad2_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(compress_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// common_change
	error = clReleaseMemObject(d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_c);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clReleaseMemObject(d_iN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_iS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_jE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_jW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clReleaseMemObject(d_dN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_dS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_dE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_dW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clReleaseMemObject(d_sums);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_sums2);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// OpenCL structures
	error = clFlush(hardware.queue);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseCommandQueue(hardware.queue);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseContext(hardware.context);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	//=====================================================================
	return RTLIB_OK;
}

void SradOCL::displayTiming(){
	logger->Warn("%.12f s, %.12f % : GET THE HARDWARE",
		(fp) (time1-time0) / 1000000,
		(fp) (time1-time0) / (fp) (time5-time0) * 100);
	logger->Warn("%.12f s, %.12f % : GET THE SOFTWARE",
		(fp) (time2-time1) / 1000000,
		(fp) (time2-time1) / (fp) (time5-time0) * 100);
	logger->Warn("%.12f s, %.12f % : ALLOCATE MEMORY",
		(fp) (time3-time2) / 1000000,
		(fp) (time3-time2) / (fp) (time5-time0) * 100);
	logger->Warn("%.12f s, %.12f % : SET THE GLOBAL WORK SIZE",
		(fp) (time4-time3) / 1000000,
		(fp) (time4-time3) / (fp) (time5-time0) * 100);
	logger->Warn("%.12f s, %.12f % : SET KERNELS ARGUMENTS",
		(fp) (time5-time4) / 1000000,
		(fp) (time5-time4) / (fp) (time5-time0) * 100);
}

void SradOCL::allocateMemory(){
	// common memory size
	mem_size = sizeof(fp) * Ne;	// get the size of float representation of input IMAGE

	// allocate memory for entire IMAGE on DEVICE
	d_I = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// allocate memory for coordinates on DEVICE
	d_iN = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_i, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_iS = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_i, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_jE = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_j, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_jW = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_j, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// allocate memory for derivatives
	d_dN = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_dS = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_dW = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_dE = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// allocate memory for coefficient on DEVICE
	d_c = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// allocate memory for partial sums on DEVICE
	d_sums = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);


	d_sums2 = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size,
			NULL, &error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
}

void SradOCL::copyInputToDevice(){
	// Image
	error = clEnqueueWriteBuffer(hardware.queue, d_I, 1, 0, mem_size, image, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// coordinates
	error = clEnqueueWriteBuffer(hardware.queue, d_iN, 1, 0, mem_size_i, iN, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clEnqueueWriteBuffer(hardware.queue, d_iS, 1, 0, mem_size_i, iS, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clEnqueueWriteBuffer(hardware.queue, d_jE, 1, 0, mem_size_j, jE, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clEnqueueWriteBuffer(hardware.queue, d_jW, 1, 0, mem_size_j, jW, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
}

void SradOCL::kernelsSetArguments(){
	// Extract Kernel - set arguments
	error = clSetKernelArg(extract_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(extract_software.kernel, 1, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// Prepare Kernel - set arguments
	error = clSetKernelArg(prepare_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(prepare_software.kernel, 1, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(prepare_software.kernel, 2, sizeof(cl_mem),
			(void *) &d_sums);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(prepare_software.kernel, 3, sizeof(cl_mem),
			(void *) &d_sums2);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// Reduce Kernel - set arguments
	error = clSetKernelArg(reduce_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(reduce_software.kernel, 3, sizeof(cl_mem),
			(void *) &d_sums);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(reduce_software.kernel, 4, sizeof(cl_mem),
			(void *) &d_sums2);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// SRAD Kernel - set arguments
	error = clSetKernelArg(srad_software.kernel, 0, sizeof(fp),
			(void *) &lambda);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 1, sizeof(int), (void *) &Nr);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 2, sizeof(int), (void *) &Nc);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 3, sizeof(long), (void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 4, sizeof(cl_mem),
			(void *) &d_iN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 5, sizeof(cl_mem),
			(void *) &d_iS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 6, sizeof(cl_mem),
			(void *) &d_jE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 7, sizeof(cl_mem),
			(void *) &d_jW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 8, sizeof(cl_mem),
			(void *) &d_dN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 9, sizeof(cl_mem),
			(void *) &d_dS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 10, sizeof(cl_mem),
			(void *) &d_dW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 11, sizeof(cl_mem),
			(void *) &d_dE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 13, sizeof(cl_mem),
			(void *) &d_c);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 14, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// SRAD2 Kernel - set arguments
	error = clSetKernelArg(srad2_software.kernel, 0, sizeof(fp),
			(void *) &lambda);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 1, sizeof(int), (void *) &Nr);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 2, sizeof(int), (void *) &Nc);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 3, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 4, sizeof(cl_mem),
			(void *) &d_iN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 5, sizeof(cl_mem),
			(void *) &d_iS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 6, sizeof(cl_mem),
			(void *) &d_jE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 7, sizeof(cl_mem),
			(void *) &d_jW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 8, sizeof(cl_mem),
			(void *) &d_dN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 9, sizeof(cl_mem),
			(void *) &d_dS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 10, sizeof(cl_mem),
			(void *) &d_dW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 11, sizeof(cl_mem),
			(void *) &d_dE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 12, sizeof(cl_mem),
			(void *) &d_c);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 13, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// Compress Kernel - set arguments
	error = clSetKernelArg(compress_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(compress_software.kernel, 1, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
}

void SradOCL::extractKernel(){
	error = clEnqueueNDRangeKernel(hardware.queue,
					extract_software.kernel,
					1,
					NULL,
					global_work_size,
					local_work_size,
					0,
					NULL,
					NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
}

void SradOCL::prepareKernel(){
	error = clEnqueueNDRangeKernel(hardware.queue,
					prepare_software.kernel,
					1,
					NULL,
					global_work_size,
					local_work_size,
					0,
					NULL,
					NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// synchronize
	// error = clFinish(hardware.queue);
	// if (error != CL_SUCCESS)
	// fatal_CL(error, __LINE__);

}

void SradOCL::reduceKernel(){
	// initial values
	blocks2_work_size = blocks_work_size;		// original number of blocks
	global_work_size2[0] = global_work_size[0];
	no = Ne;					// original number of sum elements
	mul = 1;					// original multiplier

	// loop% % %
	while (blocks2_work_size != 0) {

		// set arguments that were uptaded in this loop
		error = clSetKernelArg(reduce_software.kernel, 1, sizeof(long),
				(void *) &no);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);
		error = clSetKernelArg(reduce_software.kernel, 2, sizeof(int),
				(void *) &mul);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		error = clSetKernelArg(reduce_software.kernel, 5, sizeof(int),
				(void *) &blocks2_work_size);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		// launch kernel
		error = clEnqueueNDRangeKernel(hardware.queue,
						reduce_software.kernel,
						1,
						NULL,
						global_work_size2,
						local_work_size,
						0,
						NULL,
						NULL);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		// synchronize
		// error = clFinish(hardware.queue);
		// if (error != CL_SUCCESS)
		// fatal_CL(error, __LINE__);

		// update execution parameters
		no = blocks2_work_size;			// get current number of elements
		if (blocks2_work_size == 1) {
			blocks2_work_size = 0;
		} else {
			mul = mul * NUMBER_THREADS;				// update the increment
			blocks_x = blocks2_work_size / (int) local_work_size[0];// number of blocks
			if (blocks2_work_size % (int) local_work_size[0] != 0) {// compensate for division remainder above by adding 											// one grid
				blocks_x = blocks_x + 1;
			}
			blocks2_work_size = blocks_x;
			global_work_size2[0] = blocks2_work_size
					* (int) local_work_size[0];
		}
	}
}

void SradOCL::sradKernel(){
	// set arguments that were uptaded in this loop
	error = clSetKernelArg(srad_software.kernel, 12, sizeof(fp),
			(void *) &q0sqr);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// launch kernel
	error = clEnqueueNDRangeKernel(hardware.queue,
					srad_software.kernel,
					1,
					NULL,
					global_work_size,
					local_work_size,
					0,
					NULL,
					NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// synchronize
	// error = clFinish(hardware.queue);
	// if (error != CL_SUCCESS)
	// fatal_CL(error, __LINE__);
}

void SradOCL::srad2Kernel(){
	error = clEnqueueNDRangeKernel(hardware.queue,
					srad2_software.kernel,
					1,
					NULL,
					global_work_size,
					local_work_size,
					0,
					NULL,
					NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// synchronize
	// error = clFinish(hardware.queue);
	// if (error != CL_SUCCESS)
	// fatal_CL(error, __LINE__);
}

void SradOCL::compressKernel(){
	error = clEnqueueNDRangeKernel(hardware.queue,
					compress_software.kernel,
					1,
					NULL,
					global_work_size,
					local_work_size,
					0,
					NULL,
					NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
}
