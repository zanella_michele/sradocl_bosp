/**
 *       @file  SradOCL_main.cc
 *      @brief  The SradOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Mirko Conti mirko.conti1@mail.polimi.it
 *     @author	Michele Zanella zanella.michele@mail.polimi.it
 *
 *     Company  Polytechnic University of Milan
 *   Copyright  Copyright (c) 2014
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "SradOCL_exc.h"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

//Application Include
#include <stdlib.h>			// (in path known to compiler)	needed by malloc, free
#include "../include/main.h"		// (in current path)
#include "./util/graphics/graphics.h"	// (in specified path)
#include "./util/graphics/resize.h"	// (in specified path)
#include "./util/timer/timer.h"		// (in specified path)

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "SradOCL"

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;

/**
 * The decription of each SradOCL parameters
 */
po::options_description opts_desc("SradOCL Configuration Options");

/**
 * The map of all SradOCL parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/SradOCL.conf" ;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;


/**
 * @brief The EXecution Context (EXC) registered
 */
pBbqueEXC_t pexc;

void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "SradOCL (ver. " << g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]) {

	//=====================================================================
	// DEFINE VARIABLES
	//=====================================================================

	// image operation paths
	char image_input_path[]="../../../contrib/user/SradOCL/data/input/image.pgm";
	char image_output_path[]="../../../contrib/user/SradOCL/data/output/image_out.pgm";

	// inputs image, input paramenters
	fp* image_ori;											
	// originalinput image
	int image_ori_rows;
	int image_ori_cols;
	long image_ori_elem;

	// inputs image, input paramenters	
	fp* image;			// input image
	int Nr,Nc;			// IMAGE nbr of rows/cols/elements
	long Ne;

	// algorithm parameters
	int niter;			// nbr of iterations
	fp lambda;			// update step size

	// size of IMAGE
	int r1,r2,c1,c2;		// row/col coordinates of uniform ROI
	long NeROI;			// ROI nbr of elements

	// surrounding pixel indicies
	int* iN;
	int* iS;
	int* jE;
	int* jW;    

	// counters
	int iter = 0; // primary loop
	long i;       // image row
	long j;       // image col

	// memory sizes
	int mem_size_i;
	int mem_size_j;

	// profiling case
	int wgsize = 256;
	int awmid = -1;
	
	// timers
	long long time0;
	long long time1;
	long long time2;
	long long time3;
	long long time4;
	long long time5;
	long long time6;

	time0 = get_time();

	//=====================================================================
	// BOSP SETUP & WELCOMING
	//=====================================================================

	opts_desc.add_options()
		("help,h", "print this help message")
		("version,v", "print program version")

		("conf,C", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"SradOCL configuration file")

		("recipe,r", po::value<std::string>(&recipe)->
			default_value("SradOCL"),
			"recipe name (for all EXCs)")
		("wgsize,s", po::value<int>(&wgsize)->
			default_value(256),
			"recipe name (for all EXCs)")
		("awm,w", po::value<int>(&awmid)->
			default_value(-1),
			"AWM id to force")
	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("sradocl");

	ParseCommandLine(argc, argv);

	// Welcome screen
	logger->Info(".:: SradOCL (ver. %s) ::.", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__);


	//=====================================================================
	//INPUT ARGUMENTS
	//=====================================================================

	if(argc < 5){
		logger->Error("ERROR: wrong number of arguments\n");
		return 0;
	}
	else{
		niter = atoi(argv[1]);
		lambda = atof(argv[2]);
		Nr = atoi(argv[3]);		// it is 502 in the original image
		Nc = atoi(argv[4]);		// it is 458 in the original image
	}

	time1 = get_time();

	//=====================================================================
	// READ INPUT FROM FILE
	//=====================================================================

	// READ IMAGE (SIZE OF IMAGE HAS TO BE KNOWN)

	image_ori_rows = 502;
	image_ori_cols = 458;
	image_ori_elem = image_ori_rows * image_ori_cols;

	image_ori = (fp*)malloc(sizeof(fp) * image_ori_elem);

	read_graphics(	image_input_path,
			image_ori,
			image_ori_rows,
			image_ori_cols,
			1);

	// RESIZE IMAGE (ASSUMING COLUMN MAJOR STORAGE OF image_orig)

	Ne = Nr*Nc;

	image = (fp*)malloc(sizeof(fp) * Ne);

	resize(	image_ori,
		image_ori_rows,
		image_ori_cols,
		image,
		Nr,
		Nc,
		1);
	time2 = get_time();

	//=====================================================================
	// APPLICATION SETUP
	//=====================================================================

	// variables
	r1     = 0;		// top row index of ROI
	r2     = Nr - 1;	// bottom row index of ROI
	c1     = 0;		// left column index of ROI
	c2     = Nc - 1;	// right column index of ROI

	// ROI image size
	NeROI = (r2-r1+1)*(c2-c1+1);		// number of elements in ROI, ROI size

	// allocate variables for surrounding pixels
	mem_size_i = sizeof(int) * Nr;		
	iN = (int *)malloc(mem_size_i) ;	// north surrounding element
	iS = (int *)malloc(mem_size_i) ;	// south surrounding element
	mem_size_j = sizeof(int) * Nc;		
	jW = (int *)malloc(mem_size_j) ;	// west surrounding element
	jE = (int *)malloc(mem_size_j) ;	// east surrounding element

	// N/S/W/E indices of surrounding pixels (every element of IMAGE)
	for (i=0; i<Nr; i++) {
		iN[i] = i-1;		// holds index of IMAGE row above
		iS[i] = i+1;		// holds index of IMAGE row below
	}
	for (j=0; j<Nc; j++) {
		jW[j] = j-1;		// holds index of IMAGE column on the left
		jE[j] = j+1;		// holds index of IMAGE column on the right
	}

	// N/S/W/E boundary conditions, fix surrounding indices outside boundary of image
	iN[0]    = 0;		// changes IMAGE top row index from -1 to 0
	iS[Nr-1] = Nr-1;	// changes IMAGE bottom row index from Nr to Nr-1 
	jW[0]    = 0;		// changes IMAGE leftmost column index from -1 to 0
	jE[Nc-1] = Nc-1;	// changes IMAGE rightmost column index from Nc to Nc-1

	time3= get_time();

	//=====================================================================
	// BOSP INITIALIZATION & LAUNCH
	//=====================================================================
	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib, application [%s]...",
			::basename(argv[0]));
	RTLIB_Init(::basename(argv[0]), &rtlib);
	assert(rtlib);

	logger->Info("STEP 1. Registering EXC using [%s] recipe...",
			recipe.c_str());
	pexc = pBbqueEXC_t(new SradOCL("SradOCL", recipe, rtlib,	//std BOSP parameters
					image,				// input image
					Nr,				// IMAGE nbr of rows
					Nc,				// IMAGE nbr of cols
					Ne,				// IMAGE nbr of elem
					niter,				// nbr of iterations
					lambda,				// update step size
					NeROI,				// ROI nbr of elements
					iN,
					iS,
					jE,
					jW,
					iter,				// primary loop
					mem_size_i,
					mem_size_j,
					wgsize,
					awmid));
	if (!pexc->isRegistered())
		return RTLIB_ERROR;


	logger->Info("STEP 2. Starting EXC control thread...");
	pexc->Start();


	logger->Info("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();


	logger->Info("STEP 4. Disabling EXC...");
	pexc = NULL;

	//=====================================================================
	// TERMINATION
	//=====================================================================
	
	time4 = get_time();

	// WRITE OUTPUT IMAGE TO FILE
	write_graphics(	image_output_path,
			image,
			Nr,
			Nc,
			1,
			255);

	time5 = get_time();

	// FREE MEMORY
	free(image_ori);
	free(image);
	free(iN); 
	free(iS); 
	free(jW); 
	free(jE);

	time6 = get_time();

	// DISPLAY TIMING
	logger->Info("Time spent in different stages of the application:\n");
	logger->Info("%.12f s, %.12f % : READ COMMAND LINE PARAMETERS", 						
		(fp) (time1-time0) / 1000000, 
		(fp) (time1-time0) / (fp) (time5-time0) * 100);
	logger->Info("%.12f s, %.12f % : READ AND RESIZE INPUT IMAGE FROM FILE", 				
		(fp) (time2-time1) / 1000000, 
		(fp) (time2-time1) / (fp) (time5-time0) * 100);
	logger->Info("%.12f s, %.12f % : SETUP",
		(fp) (time3-time2) / 1000000, 
		(fp) (time3-time2) / (fp) (time5-time0) * 100);
	logger->Info("%.12f s, %.12f % : KERNEL", 												
		(fp) (time4-time3) / 1000000, 
		(fp) (time4-time3) / (fp) (time5-time0) * 100);
	logger->Info("%.12f s, %.12f % : WRITE OUTPUT IMAGE TO FILE", 	
		(fp) (time5-time4) / 1000000, 
		(fp) (time5-time4) / (fp) (time5-time0) * 100);
	logger->Info("%.12f s, %.12f % : FREE MEMORY", 											
		(fp) (time6-time5) / 1000000, 
		(fp) (time6-time5) / (fp) (time5-time0) * 100);
	logger->Info("Total time: %.12f s\n", 
		(fp) (time5-time0) / 1000000);

	// EXITING
	logger->Info("===== SradOCL DONE! =====");
	return EXIT_SUCCESS;

}

